#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "bookplayer.h"

void BookplayerPlugin::registerTypes(const char *uri) {
    //@uri Bookplayer
    qmlRegisterSingletonType<Bookplayer>(uri, 1, 0, "Bookplayer", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Bookplayer; });
}
