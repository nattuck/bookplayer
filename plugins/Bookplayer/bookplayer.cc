#include <QDebug>
#include <QTextStream>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QIODevice>
#include <QStringList>
#include <QRegularExpression>

#include <ios>
#include <iomanip>
#include <sstream>
#include <iostream>
using std::cout;
using std::endl;

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <signal.h>

#include "bookplayer.h"

QString
s2qs(std::string xx)
{
    return QString::fromStdString(xx);
}

std::string
qs2s(QString xx)
{
    return xx.toStdString();
}

Bookplayer::Bookplayer()
    : timer_id(0), file_name(""), position(0.0)
{
    qDebug() << "constructor start";

    setenv("APP_ID", "bookplayer", 0);
    setenv("XDG_DATA_HOME", "/tmp", 0);

    qDebug() << "constructor P";
    // Create directories
    if (!QDir::root().mkpath(data_path())) {
        qDebug() << "failed to make data dir";
    }
    if (!QDir::root().mkpath(books_path())) {
        qDebug() << "failed to make books dir";
    }

    load_state();

    player.setNotifyInterval(100);

    connect(&player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
            this, SLOT(onMediaStatusChanged(QMediaPlayer::MediaStatus)));
    connect(&player, SIGNAL(stateChanged(QMediaPlayer::State)),
            this, SLOT(onStateChanged(QMediaPlayer::State)));
    connect(&player, SIGNAL(positionChanged(qint64)),
            this, SLOT(onPositionChanged(qint64)));

    qDebug() << "constructor done";
}

QString
Bookplayer::get_name()
{
    return s2qs(file_name);
}

std::string
format_time(double dsecs)
{
    long secs = (long)(dsecs) % 60;
    long mins = (long)(dsecs / 60);
    std::stringstream temp;
    temp << mins;
    temp << ":";
    temp.fill('0');
    temp.width(2);
    temp << secs;
    return temp.str();
}

QString
Bookplayer::get_time()
{
    double dur = player.duration() / 1000.0;
    std::stringstream temp;
    temp << format_time(get_pos());
    temp << " / ";
    temp << format_time(dur);
    return s2qs(temp.str());
}

double
Bookplayer::get_pos()
{
    return position;
}

void
Bookplayer::load()
{
    qDebug() << "hello world!";
    qDebug() << "data = " << data_path();
    qDebug() << "books = " << books_path();
    qDebug() << "book list = \n" << list_books().join("\n");

    load_state();
}

void
Bookplayer::play()
{
    qDebug() << "play";
    qDebug() << "position = " << position;
    timer_id = startTimer(5000);
    player.setVolume(50);
    player.play();
}

void
Bookplayer::pause()
{
    qDebug() << "pause";
    killTimer(timer_id);
    player.pause();
    save_state();
}

void
Bookplayer::seek(double pos)
{
    position = pos;
    next_pos_ms = (long)(pos * 1000.0);
    if (player.state() == QMediaPlayer::PlayingState) {
        player.setPosition(next_pos_ms);
        changed_pos = false;
    }
    else {
        changed_pos = true;
    }
}

void
Bookplayer::seek_by(double delta)
{
    seek(position + delta);
}

void
Bookplayer::change_chapter(int delta)
{
    QStringList cs = list_files();
    cs.sort();
    int size = cs.size();

    if (size == 0) {
        qDebug() << "there are no chapters";
        return;
    }

    QRegularExpression re(QRegularExpression::escape(s2qs(file_name)));
    int curr = cs.indexOf(re);
    if (curr == -1) {
        qDebug() << "current chapter missing";
        set_file(cs[0]);
        return;
    }

    int next = curr + delta;
    if (next < 0 || next >= size) {
        qDebug() << "no more chapters";
        return;
    }

    set_file(cs[next]);
}

void
Bookplayer::load_state()
{
    qDebug() << "start load_state";

    QString path = Path(data_path()).join("state.txt").q_str();
    QFile file(path);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream stm(&file);
        book_name = qs2s(stm.readLine());
        file_name = qs2s(stm.readLine());

        QString posn = stm.readLine();
        qDebug() << "posn read: " << posn;
        position = atof(qs2s(posn).c_str());
        file.close();

        Path new_file = Path(data_path()).join(file_name);
        player.setMedia(QUrl::fromLocalFile(new_file.q_str()));

        qint64 ms = qint64(position * 1000.0);
        qDebug() << "ms: " << ms;

        // Queue a seek.
        next_pos_ms = ms;
        changed_pos = true;
        //player.setPosition(ms);
        //qDebug() << "player pos: " << player.position();
    }
    else {
        qDebug() << "state file not opened";
        file_name = "";
        position = 0.0;
    }

    qDebug() << "state loaded:";
    qDebug() << "book: " << s2qs(book_name);
    qDebug() << "file: " << s2qs(file_name);
    qDebug() << "time: " << position;
}

void
Bookplayer::save_state()
{
    QString path = Path(data_path()).join("state.txt").q_str();
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        return;
    }

    QTextStream stm(&file);
    stm << s2qs(book_name) << "\n";
    stm << s2qs(file_name) << "\n";
    stm << position << "\n";
    file.close();

    qDebug() << "state saved:";
    qDebug() << "book: " << s2qs(book_name);
    qDebug() << "file: " << s2qs(file_name);
    qDebug() << "time: " << position;
}

QString
Bookplayer::data_path()
{
    std::string app_id(getenv("APP_ID"));
    std::string base(getenv("XDG_DATA_HOME"));
    std::string pkg_name = app_id.substr(0, app_id.find_first_of("_"));
    return Path(base).join(pkg_name).q_str();
}

QString
Bookplayer::books_path()
{
    //QString home = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString home(getenv("HOME"));
    return Path(home).join("Documents").join("Audiobooks").q_str();
}

QString
Bookplayer::book_path(QString name)
{
    return Path(books_path()).join(name).q_str();
}

QStringList
list_visible(QString path)
{
    QDir dd(path);
    QStringList xs = dd.entryList();
    QStringList ys;

    for (auto it = xs.begin(); it != xs.end(); ++it) {
        QString item = *it;

        QRegularExpression img_ext("\\.jpg$");
        if (item.size() == 0 || item[0] == QChar('.') || item.contains(img_ext)) {
            continue;
        }

        ys << item;
    }

    return ys;
}

QStringList
Bookplayer::list_books()
{
    return list_visible(books_path());
}

void
Bookplayer::set_book(QString name)
{
    std::string name1 = qs2s(name);
    if (book_name != name1) {
        book_name = name1;
        QStringList files = list_files();
        set_file(files[0]);
    }
}

QString
Bookplayer::get_book()
{
    return s2qs(book_name);
}

QStringList
Bookplayer::list_files()
{
    return list_visible(book_path(s2qs(book_name)));
}

void
Bookplayer::set_file(QString name)
{
    int rv;

    qDebug() << "set file: " << name;

    Path data_dir(data_path());
    Path books_dir(books_path());

    // Unlink previous file
    if (file_name != "") {
        std::string old_file = data_dir.join(file_name).to_string();
        cout << "unlink(" << old_file << ")" << endl;
        rv = unlink(old_file.c_str());
        if (rv == -1) {
            file_name = std::string("");
            perror("unlink");
            return;
        }
    }

    // Link in new file.
    file_name = name.toStdString();

    Path src_path = books_dir.join(book_name).join(file_name);
    Path new_file = data_dir.join(file_name);
    cout << "copy(" << src_path.to_string() << ", "
         << new_file.to_string() << ")" << endl;
    bool copy_ok = QFile::copy(src_path.q_str(), new_file.q_str());
    if (copy_ok) {
        cout << "copy ok" << endl;
    }
    else {
        file_name = std::string("");
        cout << "copy failed" << endl;
        return;
    }

    // Save state.
    position = 0.0;
    save_state();

    // Set current file.
    cout << "set file = " << new_file.to_string() << endl;
    player.setMedia(QUrl::fromLocalFile(new_file.q_str()));
}

QString
Bookplayer::file_path(QString name)
{
    return Path(book_path(s2qs(book_name))).join(name).q_str();
}

void
Bookplayer::timerEvent(QTimerEvent* event)
{
    qDebug() << "tick";
    if (player.state() == QMediaPlayer::State::PlayingState) {
        position = player.position() / 1000.0;
        qDebug() << "position: " << qSetRealNumberPrecision(2) << position;
        save_state();
    }
}

void
Bookplayer::onMediaStatusChanged(QMediaPlayer::MediaStatus status)
{
    if (status == QMediaPlayer::EndOfMedia) {
        qDebug() << "chapter done";

        change_chapter(1);
        play();
    }
}

void
Bookplayer::onStateChanged(QMediaPlayer::State state)
{
    if (state == QMediaPlayer::PlayingState) {
        if (changed_pos) {
            player.setPosition(next_pos_ms);
            changed_pos = false;
        }

        qDebug() << "now playing";
    }

    if (state == QMediaPlayer::PausedState) {
        qDebug() << "now paused";
    }
}

void
Bookplayer::onPositionChanged(qint64 pos_ms)
{
    position = pos_ms / 1000.0;
}
