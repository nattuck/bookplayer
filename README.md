# BookPlayer

This is an extremely minimal audiobook player for phones running Ubuntu Touch.

## Installation

 * Install [Clickable](http://clickable.bhdouglass.com/en/latest/)
 * Build and run on your device with clickable.
 * App will then be installed.
 * Install UT Tweak Tool and disable this app being suspended when
   it's not in the foreground.

## Usage

 * Create a directory: ~/Documents/Audiobooks
 * Put audiobooks there.
 * An audiobook is:
   * A directory
   * Containing audio files (mp3 and m4b seem to work).
   * Alphabetical sorting by file name is chapter order.
 * Run the app and press the right buttons.

## Copyright

Copyright (C) 2019 Nat Tuck. License: GNU GPLv3 or any later version published
by the FSF.

