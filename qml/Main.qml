import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import Bookplayer 1.0
import QtMultimedia 5.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'Book Player'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    PageStack {
        id: mainPageStack

        Page {
            id: pickerPage
            anchors.fill: parent

            header: PageHeader {
                id: header
                title: i18n.tr('Pick a Book')
            }

            ListModel {
                id: listModel
            }

            ListView {
                anchors.topMargin: header.height
                anchors.fill: parent

                model: listModel
                delegate: Button {
                    text: name
                    Layout.margins: 5
                    onClicked: {
                        Bookplayer.set_book(name);
                        mainPageStack.push(Qt.resolvedUrl("PlayerPage.qml"));
                    }
                }
            }

            Component.onCompleted: {
                listModel.clear();

                var books = Bookplayer.list_books().map(function (book) {
                    listModel.append({ name: book });
                });

                if (Bookplayer.get_book() != "") {
                    mainPageStack.push(Qt.resolvedUrl("PlayerPage.qml"));
                }
            }
        }

        Component.onCompleted: {
            Bookplayer.load()
        }
    }
}
