
#include <alloca.h>
#include <libgen.h>
#include <string.h>
#include <sstream>
#include <iostream>

#include "path.h"

using std::string;
using std::vector;
using std::stringstream;
using std::cout;
using std::endl;

Path::Path(const char* text)
    : data(text)
{
    // do nothing
}

Path::Path(string text)
    : data(text)
{
    // do nothing
}

Path::Path(QString text)
    : data(text.toStdString())
{
    // do nothing
}

Path::Path(std::vector<std::string> parts, bool absolute)
    : data("")
{
    stringstream temp;
    if (absolute) {
        temp << "/";
    }

    for (long ii = 0; ii < parts.size(); ++ii) {
        temp << parts[ii];
        if (ii + 1 < parts.size()) {
            temp << "/";
        }
    }

    data = temp.str();
}

Path
Path::dir_name()
{
    char* temp = (char*) alloca(data.size() + 2);
    strcpy(temp, data.c_str());
    return Path(dirname(temp));
}

string
Path::base_name()
{
    cout << "in basename" << endl;
    char* temp = (char*) alloca(data.size() + 2);
    strcpy(temp, data.c_str());
    return string(basename(temp));
}

bool
Path::is_absolute()
{
    return data.size() > 0 && data[0] == '/';
}

std::vector<string>
Path::parts()
{
    std::vector<string> ys;
    string temp = data;
    while (temp.size() > 0) {
        while (temp[0] == '/') {
            temp = temp.substr(1);
        }

        auto ii = temp.find_first_of("/");
        if (ii != string::npos) {
            string part = temp.substr(0, ii);
            ys.push_back(part);
            temp = temp.substr(ii);
        }
        else {
            ys.push_back(temp);
            temp = string("");
        }
    }
    return ys;
}

Path
Path::join(Path pp)
{
    std::vector<string> zs;

    auto xs = parts();
    zs.insert(zs.end(), xs.begin(), xs.end());

    auto ys = pp.parts();
    zs.insert(zs.end(), ys.begin(), ys.end());

    Path result(zs, is_absolute());
    return result;
}

Path
Path::join(const char* text)
{
    return join(Path(text));
}

Path
Path::join(std::string text)
{
    return join(Path(text));
}

Path
Path::join(QString text)
{
    return join(Path(text));
}

const char*
Path::c_str()
{
    return data.c_str();
}

QString
Path::q_str()
{
    return QString::fromStdString(data);
}

std::string
Path::to_string()
{
    return data;
}

