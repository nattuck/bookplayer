#ifndef BOOKPLAYERPLUGIN_H
#define BOOKPLAYERPLUGIN_H

#include <QQmlExtensionPlugin>

class BookplayerPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
