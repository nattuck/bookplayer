#ifndef BOOKPLAYER_H
#define BOOKPLAYER_H

#include <string>

#include <QObject>
#include <QString>
#include <QtMultimedia/QMediaPlayer>

#include "path.h"

class Bookplayer: public QObject {
    Q_OBJECT

public:
    Bookplayer();
    ~Bookplayer() = default;

    Q_INVOKABLE void load();

    Q_INVOKABLE QString data_path();
    Q_INVOKABLE QString books_path();

    Q_INVOKABLE QStringList list_books();
    Q_INVOKABLE void set_book(QString name);
    Q_INVOKABLE QString get_book();
    Q_INVOKABLE QString book_path(QString name);

    Q_INVOKABLE QStringList list_files();
    Q_INVOKABLE void set_file(QString name);
    Q_INVOKABLE QString file_path(QString name);

    Q_INVOKABLE void play();
    Q_INVOKABLE void pause();
    Q_INVOKABLE void seek(double pos);
    Q_INVOKABLE void seek_by(double delta);
    Q_INVOKABLE void change_chapter(int delta);

    Q_INVOKABLE QString get_name();
    Q_INVOKABLE QString get_time();
    Q_INVOKABLE double  get_pos();

public slots:
    void onMediaStatusChanged(QMediaPlayer::MediaStatus status);
    void onStateChanged(QMediaPlayer::State state);
    void onPositionChanged(qint64 pos_ms);

protected:
    QMediaPlayer player;
    int timer_id;

    // State: current file info
    std::string book_name;   // book
    std::string file_name;   // chapter
    double position;

    // Queue a position change.
    long next_pos_ms;
    bool changed_pos;

    void timerEvent(QTimerEvent* event);
    void load_state();
    void save_state();
};

#endif
