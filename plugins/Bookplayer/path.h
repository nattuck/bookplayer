#ifndef BOOKPLAYER_PATH_H
#define BOOKPLAYER_PATH_H

#include <vector>
#include <string>
#include <QString>

class Path {
public:
    Path(const char* text);
    Path(std::string text);
    Path(QString text);
    Path(std::vector<std::string> parts, bool absolute = false);

    Path dir_name();
    std::string base_name();

    bool is_absolute();
    std::vector<std::string> parts();

    Path join(Path pp);
    Path join(const char* text);
    Path join(std::string text);
    Path join(QString text);

    const char* c_str();
    QString q_str();
    std::string to_string();

protected:
    std::string data;
};

#endif
