import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import Bookplayer 1.0

Page {
    id: playerPage

    header: PageHeader {
        id: header
        title: "Playing: ..."
    }

    Component.onCompleted: {
        viewTimer.start()
    }

    Timer {
        id: viewTimer
        interval: 500
        running: false
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            //console.log("ui tick");
            header.title = "Playing: " + Bookplayer.get_name();
            nameText.text = Bookplayer.get_name();
            timeText.text = Bookplayer.get_time();
        }
    }

    Rectangle {
        anchors.fill: parent
        anchors.topMargin: header.height

        Grid {
            anchors.fill: parent

            columns: 3
            spacing: 5

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                Button {
                    anchors.centerIn: parent
                    text: "pick book"

                    onClicked: {
                        Bookplayer.pause();
                        mainPageStack.pop();
                    }
                }
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                Button {
                    anchors.centerIn: parent
                    text: "<< Prev"
                    onClicked: {
                        Bookplayer.change_chapter(-1);
                    }
                }
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                Label {
                    width: parent.width
                    height: parent.height

                    id: nameText
                    textSize: Label.Large
                    wrapMode: Text.Wrap
                }
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                Button {
                    anchors.centerIn: parent
                    text: "Next >>"
                    onClicked: {
                        Bookplayer.change_chapter(1);
                    }
                }
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                Column {
                    anchors.centerIn: parent
                    spacing: 5

                    Button {
                        text: "<< -10s"

                        onClicked: {
                            Bookplayer.seek_by(-10.0);
                        }
                    }

                    Button {
                        text: "<< -1m"

                        onClicked: {
                            Bookplayer.seek_by(-60.0);
                        }
                    }
                }
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                // TODO: Figure out how to update timestamp.

                Label {
                    id: timeText
                    anchors.centerIn: parent
                    text: "0:00:00"
                }
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                Column {
                    anchors.centerIn: parent
                    spacing: 5

                    Button {
                        text: "+10s >>"

                        onClicked: {
                            Bookplayer.seek_by(10.0);
                        }
                    }

                    Button {
                        text: "+1m >>"

                        onClicked: {
                            Bookplayer.seek_by(60.0);
                        }
                    }
                }
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                Button {
                    anchors.centerIn: parent
                    text: "pause"

                    onClicked: {
                        Bookplayer.pause();
                    }
                }
            }

            Rectangle {
                width: parent.width / 4
                height: parent.height / 4

                Button {
                    anchors.centerIn: parent
                    text: "play"

                    onClicked: {
                        Bookplayer.play();
                    }
                }
            }
 
        }
    }
}
